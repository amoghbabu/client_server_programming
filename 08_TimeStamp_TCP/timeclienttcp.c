//  TCP/IP Time client
//----------------------

#include<time.h>
#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define UNIXEPOCH 22089


/*  Global constants  */

#define MAX_LINE           (1000)


int main() 
{

    int       n;
    int       conn_s;                /*  connection socket         */
    short int port;                  /*  port number               */
    struct    sockaddr_in servaddr;  /*  socket address structure  */
    char      buffer[MAX_LINE];      /*  character buffer          */
    char     *szAddress;             /*  Holds remote IP address   */
    char     *szPort;                /*  Holds remote port         */
    char     *endptr;                /*  for strtol()              */
    time_t  now;



    port = 5000;
    
    /*  Create the listening socket  */

    if ( (conn_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
	 {
	fprintf(stderr, "TIMECLNT: Error creating listening socket.\n");
	exit(EXIT_FAILURE);
	  }


    /*  Set all bytes in socket address structure to
        zero, and fill in the relevant data members   */

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(port);
    servaddr.sin_addr.s_addr = INADDR_ANY;

     /*  connect() to the remote time server  */

    if ( connect(conn_s, (struct sockaddr *) &servaddr, sizeof(servaddr) ) < 0 ) {
	printf("TIMECLNT: Error calling connect()\n");
	exit(EXIT_FAILURE);
    }


    /*  Retrieve current time from server  */

    printf("The current time is: ");
  
    n = read(conn_s, (char *)&now, sizeof(now));

	now = ntohl((u_long)now);
  	now -= UNIXEPOCH;
	printf(".... %ld",now);
	printf("%s", ctime(&now));
	exit(0);
}


